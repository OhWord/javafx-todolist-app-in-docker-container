#Run image of OpenJDK on Desbian Buster
FROM openjdk:14-buster

#The line below contains all the solutions I've tried online. Unfortunately the program works with this line included or not included
RUN apt-get update && apt-get install -y libgtk-3-dev

#Open ports so container can communicate to server
EXPOSE 3306
EXPOSE 1433

#Copy in JavaFX SDK and Java program jar
COPY library/linux/javafx-sdk-14.0.2.1 javafx-sdk-14.0.2.1
COPY out/artifacts/toDoList_OpenSQL_jar/toDoList-OpenSQL.jar toDoList-OpenSQL.jar
COPY assets assets

#Copy BjorkFont into Container. Although XcXsrv cannot display this font, we can just imagine that this desbian image is loading the font in our imagination ;(
RUN mkdir -p /home/jovyan/.fonts
COPY library/fonts/BjorkFont.TTF /usr/share/fonts/truetype/BjorkFont.TTF
RUN fc-cache -f -v
RUN rm -fr ~/.cache/matplotlib

#Run jar with JavaFX SDK module
ENTRYPOINT java --module-path /javafx-sdk-14.0.2.1/lib --add-modules javafx.controls,javafx.fxml -jar toDoList-OpenSQL.jar