package sample.storage;

import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class currentUserHandler {
    //INSTANCE VARIABLES
    String fileName;
    String currentUserID;

    //CONSTRUCTER
    public currentUserHandler(){
        fileName = "assets/loggedInUser.txt";
        try{
            String text = Files.readString(Paths.get(fileName));

            if(!text.equals("")) {
                String loggedInUserID = text; //gets the logged in user
                currentUserID = loggedInUserID;
            }else{
                currentUserID = "0";
            }
        } catch (FileNotFoundException e){
            JOptionPane.showMessageDialog(null, "Error Setting Logged in User");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //GETTERS
    public String getCurrentUserID(){
        return this.currentUserID;
    }

    //SETTERS
    public void setCurrentUserID(String newID){
        this.currentUserID = newID;
        try{
            FileWriter fw  = new FileWriter(fileName, false);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);

            pw.print(newID);
            System.out.println("Current User set to "+ newID);

            pw.flush();
            pw.close();
        }catch (Exception E){
            JOptionPane.showMessageDialog(null, "LoggedInUser Not Saved");
        }
    }


}
