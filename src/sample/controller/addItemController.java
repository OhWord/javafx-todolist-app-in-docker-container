package sample.controller;

import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javafx.beans.Observable;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import sample.Database.DatabaseHandler;
import sample.Model.task;
import sample.Model.user;
import sample.animations.shaker;
import sample.storage.currentUserHandler;

public class addItemController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label addItemHeader;

    @FXML
    private Button addItemLogOut;

    @FXML
    private TableView<task> addItemTasks;


    @FXML
    private TableColumn<task, String> duedate;

    @FXML
    private TableColumn<task, String> description;

    @FXML
    private Button addItemButton;

    @FXML
    private Button addItemRemoveButton;

    @FXML
    private DatePicker addItemDueDate;

    @FXML
    private TextField addItemTextField;

    ObservableList<task> taskList = FXCollections.observableArrayList();

    @FXML
    void initialize() {
        refreshTable();

        addItemButton.setOnAction(actionEvent ->{
            if(!(addItemTextField.getText()=="") && !(addItemDueDate.getValue()==null)) {
                createTask();
                addItemTextField.clear();
                addItemDueDate.setValue(null);
                refreshTable();
            }else{
                shaker tShaker = new shaker(addItemTextField);
                tShaker.shake();

                shaker dShaker = new shaker(addItemDueDate);
                dShaker.shake();

                shaker aShaker = new shaker(addItemButton);
                aShaker.shake();
            }
        });

        addItemRemoveButton.setOnAction(actionEvent -> {
            ObservableList<task> selectedTasks;
            selectedTasks = addItemTasks.getSelectionModel().getSelectedItems();
            DatabaseHandler databaseHandler = new DatabaseHandler();
            for(task curTask : selectedTasks){
                databaseHandler.removeTaskByTaskID(databaseHandler.getTaskIDby(curTask.getDuedate(), curTask.getDescription()));;
            }
            refreshTable();
        });

        addItemLogOut.setOnAction(actionEvent -> {
            closeWindow();
            currentUserHandler currentUserHandler = new currentUserHandler();
            currentUserHandler.setCurrentUserID("0");
        });

    }

    public void createTask(){
        DatabaseHandler genesisU = new DatabaseHandler();

        String dueDate = addItemDueDate.getValue()+"";
        String description = addItemTextField.getText();

        //Format DueDate
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(dueDate);
            dueDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        task task = new task(dueDate, description);

        genesisU.addTask(task);
    }

    public void refreshTable(){
        /*
        duedate = new TableColumn("DUEDATE");
        description = new TableColumn("DESCRIPTION");
        addItemTasks.getColumns().addAll(duedate,description);

         */

        DatabaseHandler databaseHandler = new DatabaseHandler();
        currentUserHandler currentUserHandler = new currentUserHandler();

        taskList = databaseHandler.getTaskOListbyUserID(currentUserHandler.getCurrentUserID());

        System.out.println("Size of taskList: "+taskList.size());


        duedate.setCellValueFactory(new PropertyValueFactory<task, String>("duedate"));

        description.setCellValueFactory(new PropertyValueFactory<task, String>("description"));

        addItemTasks.setItems(taskList);
    }

    private void closeWindow(){
        // get a handle to the stage
        Stage stage = (Stage) addItemLogOut.getScene().getWindow();
        // do what you have to do
        stage.close();
    }
}