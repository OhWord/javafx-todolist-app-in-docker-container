package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import sample.Database.DatabaseHandler;
import sample.Model.user;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class signUpController {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label signUpHeader;

    @FXML
    private Button signUpBackButton;

    @FXML
    private TextField signUpFirstName;

    @FXML
    private TextField signUpUsername;

    @FXML
    private TextField signUpLastName;

    @FXML
    private TextField signUpLocation;

    @FXML
    private CheckBox signUpCheckBoxMale;

    @FXML
    private CheckBox signUpCheckBoxFemale;

    @FXML
    private TextField signUpPassowrd;

    @FXML
    private Button signUpLoginButton;

    @FXML
    void initialize() {

        signUpLoginButton.setOnAction(event -> {
            createUser();

            try {
                showAddItemScreen();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        signUpBackButton.setOnAction(event -> {
            try {
                showLoginScreen();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
    private void showAddItemScreen() throws IOException {
        signUpLoginButton.getScene().getWindow().hide(); //hide this window

        //LOADER
        Parent root = FXMLLoader.load(getClass().getResource("/sample/view/addItem.fxml"));
        Stage stage = new Stage();
        stage.setScene(new Scene(root, 700, 400));
        stage.show();
    }
    private void showLoginScreen() throws IOException {
        signUpBackButton.getScene().getWindow().hide(); //hide this window

        //LOADER
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/sample/view/login.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage stage = new Stage();
        stage.setTitle("To Do List");
        stage.getIcons().add(new Image("/sample/assets/DondaAlbumCover.PNG"));
        stage.setScene(new Scene(root, 700, 400));
        stage.show();
    }
    public void createUser(){
        DatabaseHandler genesisU = new DatabaseHandler();

        String firstName = signUpFirstName.getText();
        String lastName = signUpLastName.getText();
        String userName = signUpUsername.getText();
        String password = signUpPassowrd.getText();
        String location = signUpLocation.getText();
        String gender;

        if(signUpCheckBoxFemale.isSelected()){
            gender = "Female";
        }else {
            gender = "Male";
        }

        user user = new user(firstName,lastName,userName,password,location, gender);
        //currentUserHandler currentUserHandler = new currentUserHandler();
        //currentUserHandler.setCurrentUserID();

        genesisU.signUpUser(user);
    }
}
