package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import sample.Database.DatabaseHandler;
import sample.Model.user;
import sample.animations.shaker;
import sample.storage.currentUserHandler;

import java.io.IOException;

public class loginController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label loginHeaderLabel;

    @FXML
    private TextField loginUsername;

    @FXML
    private TextField loginPassowrd;

    @FXML
    private Button loginButton;

    @FXML
    private Button loginSignUpButton;

    @FXML
    private Label loginErrorTextfield;

    DatabaseHandler databaseHandler;

    @FXML
    void initialize() {

        databaseHandler = new DatabaseHandler();
        currentUserHandler currentUserHandler = new currentUserHandler();
        currentUserHandler.setCurrentUserID("0");

        loginButton.setOnAction(event -> {
            String loginText = loginUsername.getText().trim();
            String loginPassword = loginPassowrd.getText().trim();

            user user = new user();
            user.setUsername(loginText);
            user.setPassword(loginPassword);

            ResultSet userRow = databaseHandler.getUserResultSet(user);

            int counter = 0;
            if(!loginUsername.getText().equals("") && !loginPassowrd.getText().equals("")) {
                try {
                    String theUID = "0";
                    while (userRow.next()) {
                        counter++;
                        theUID = userRow.getInt(1)+"";
                    }
                    if (counter == 1) {
                        System.out.println("Login Successful!");
                        currentUserHandler.setCurrentUserID(theUID);
                        showAddItemScreen();
                    } else {
                        //Invalid Username/Password
                        shaker uShaker = new shaker(loginUsername);
                        uShaker.shake();

                        shaker pShaker = new shaker(loginPassowrd);
                        pShaker.shake();
                        loginErrorTextfield.setText("Invalid Username/Password");
                    }

                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }else {
                //Blank Username/Password
                shaker uShaker = new shaker(loginUsername);
                uShaker.shake();

                shaker pShaker = new shaker(loginPassowrd);
                pShaker.shake();
                loginErrorTextfield.setText("Please enter a Username and Password");
            }
        });

        loginSignUpButton.setOnAction(actionEvent -> {
            //Take Users to sign up Screen
            loginSignUpButton.getScene().getWindow().hide(); //hide this window
            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/sample/view/signUp.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Stage stage = new Stage();
            stage.setTitle("To Do List");
            stage.getIcons().add(new Image("/sample/assets/DondaAlbumCover.PNG"));
            stage.setScene(new Scene(root, 700, 400));
            stage.show();
        });
    }

    public void setLabelStyle(Label label) {
        Font bjorkFont = Font.loadFont("/sample/assets/BjorkFont.TTF", 10);
        label.setFont(bjorkFont);
    }

    private void showAddItemScreen() throws IOException {
        loginSignUpButton.getScene().getWindow().hide(); //hide this window

        //LOADER
        Parent root = FXMLLoader.load(getClass().getResource("/sample/view/addItem.fxml"));
        Stage stage = new Stage();
        stage.setTitle("To Do List");
        stage.getIcons().add(new Image("/sample/assets/DondaAlbumCover.PNG"));
        stage.setScene(new Scene(root, 700, 400));
        stage.show();
    }
}