package sample.Model;

public class task {
    //Instance Variables
    private String duedate;
    private String description;


    //Constructor
    public task(String duedate, String description){
        this.duedate = duedate;
        this.description = description;
    }

    //Getters
    public String getDuedate(){
        return duedate;
    }
    public String getDescription(){
        return description;
    }

    //Setters
    public void setDuedate(String inDuedate){
        duedate = inDuedate;
    }
    public void setDescription(String inDescription){
        description = inDescription;
    }
}
