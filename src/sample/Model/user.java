package sample.Model;

public class user {
    //Instance Variables
    String firstName;
    String lastName;
    String username;
    String password;
    String location;
    String gender;

    //Constructor
    public user(){

    }

    public user(String firstName, String lastName, String username, String password, String location, String gender){
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.location = location;
        this.gender = gender;
    }

    //Getters
    public String getFirstName(){
        return this.firstName;
    }
    public String getLastName(){
        return this.lastName;
    }
    public String getUsername(){
        return this.username;
    }
    public String getPassword(){
        return this.password;
    }
    public String getLocation(){
        return this.location;
    }
    public String getGender(){
        return this.gender;
    }

    //Setters
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public void setGender(String gender){
        this.gender = gender;
    }
}
