package sample.Database;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.Model.task;
import sample.Model.user;
import sample.storage.currentUserHandler;

import java.sql.*;

public class DatabaseHandler extends Configs {
    Connection dbConnection;

    public Connection getDbConnection() throws ClassNotFoundException, SQLException {
        String connectionString = "jdbc:mySQL://"+ dbHost + ":" + dbPort + "/" + dbName;
        Class.forName("com.mysql.jdbc.Driver");
        dbConnection = DriverManager.getConnection(connectionString, dbUser, dbPass);
        return dbConnection;
    }

    //Write
    public void signUpUser(user user) {
        String insert = "INSERT INTO "+Const.USERS_TABLE + "(" + Const.USERS_FIRSTNAME + "," + Const.USERS_LASTNAME + "," + Const.USERS_USERNAME + "," + Const.USERS_PASSWORD + "," + Const.USERS_LOCATION + "," + Const.USERS_GENDER + ")" + "VALUES(?,?,?,?,?,?)";
        try{
            PreparedStatement preparedStatement =getDbConnection().prepareStatement(insert);

            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getUsername());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getLocation());
            preparedStatement.setString(6, user.getGender());

            preparedStatement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void addTask(task task) {
        currentUserHandler currentUserHandler = new currentUserHandler();

        String insert = "INSERT INTO "+Const.TASKS_TABLE + "(" + Const.TASKS_UID + "," + Const.TASKS_DUEDATE + "," + Const.TASKS_DESCRIPTION + ")" + "VALUES(?,?,?)";
        try{
            PreparedStatement preparedStatement =getDbConnection().prepareStatement(insert);

            preparedStatement.setString(1, currentUserHandler.getCurrentUserID());
            preparedStatement.setString(2, task.getDuedate());
            preparedStatement.setString(3, task.getDescription());

            preparedStatement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //Read
    public ResultSet getUserResultSet (user user) {
        ResultSet resultSet = null;

        if(!user.getUsername().equals("") && !user.getPassword().equals("")) {
            String query = "SELECT * FROM " + Const.USERS_TABLE + " WHERE " + Const.USERS_USERNAME + "=?" + " AND " + Const.USERS_PASSWORD + "=?";

            try {
                PreparedStatement preparedStatement = getDbConnection().prepareStatement(query);
                preparedStatement.setString(1, user.getUsername());
                preparedStatement.setString(2, user.getPassword());

                resultSet = preparedStatement.executeQuery();

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }else{
            System.out.println("Please Enter Your Credentials");
        }

        return resultSet;
    }
    public ObservableList getTaskOListbyUserID (String UID) {
        ObservableList<task> allTasksByUser = FXCollections.observableArrayList();

        ResultSet resultSet = null;

        String query = "SELECT * FROM " + Const.TASKS_TABLE + " WHERE " + Const.TASKS_UID + "=?";

        try {
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(query);
            preparedStatement.setString(1, UID);

            resultSet = preparedStatement.executeQuery();

        } catch (SQLException throwables) {
                throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
                e.printStackTrace();
        }
        try {
            while (resultSet.next()) {
                task tempTask = new task(resultSet.getString(3) + "", resultSet.getString(4) + "");
                allTasksByUser.add(tempTask);
            }
        }catch(SQLException e){
            System.out.println("DatabaseHandler.getTaskOListbyUserID(): ERROR");
            e.printStackTrace();
        }
        return allTasksByUser;
    }
    public String getTaskIDbyTask (task task) {
        currentUserHandler currentUserHandler = new currentUserHandler();
        ResultSet resultSet = null;

        String query = "SELECT * FROM " + Const.TASKS_TABLE + " WHERE " + Const.TASKS_UID + "=?" + " AND " + Const.TASKS_DUEDATE + "=?" + " AND " + Const.TASKS_DESCRIPTION + "=?";

        try {
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(query);
            preparedStatement.setString(1, currentUserHandler.getCurrentUserID());
            preparedStatement.setString(2, task.getDuedate());
            preparedStatement.setString(3, task.getDescription());

            resultSet = preparedStatement.executeQuery();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            return resultSet.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0+"";
    }
    public String getTaskIDby (String dueDate, String description) {
        currentUserHandler currentUserHandler = new currentUserHandler();
        ResultSet resultSet = null;

        String query = "SELECT * FROM " + Const.TASKS_TABLE + " WHERE " + Const.TASKS_UID + "=?" + " AND " + Const.TASKS_DUEDATE + "=?" + " AND " + Const.TASKS_DESCRIPTION + "=?";

        try {
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(query);
            preparedStatement.setString(1, currentUserHandler.getCurrentUserID());
            preparedStatement.setString(2, dueDate);
            preparedStatement.setString(3, description);

            resultSet = preparedStatement.executeQuery();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            while(resultSet.next()) {
                return resultSet.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0+"";
    }



    //Delete
    public void removeTaskByTaskID (String taskID) {
        try {
            String SQL = "DELETE FROM tasks WHERE taskid = "+taskID+" ";
            PreparedStatement preparedStatement = getDbConnection().prepareStatement(SQL);
            preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }

    }
}
