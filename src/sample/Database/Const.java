package sample.Database;

public class Const {
    public static final String USERS_TABLE = "users";
    public static final String TASKS_TABLE = "tasks";

    //USERS TABLE Column Names
    public static final String USERS_ID = "userid";
    public static final String USERS_FIRSTNAME = "firstname";
    public static final String USERS_LASTNAME = "lastname";
    public static final String USERS_USERNAME = "username";
    public static final String USERS_PASSWORD = "password";
    public static final String USERS_LOCATION = "location";
    public static final String USERS_GENDER = "gender";

    //TASKS TABLE Column Names
    public static final String TASKS_ID = "taskid";
    public static final String TASKS_UID = "userid";
    public static final String TASKS_DUEDATE = "duedate";
    public static final String TASKS_DESCRIPTION = "description";
}

